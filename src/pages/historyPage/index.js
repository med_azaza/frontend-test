import styles from "./History.module.scss";
import React, { useEffect, useState } from "react";
export default function History() {
  const [list, setList] = useState([]);
  useEffect(() => {
    setList(JSON.parse(localStorage.getItem("history")));
  }, []);
  return (
    <div className={styles.container}>
      {list.map((el) => (
        <div key={el.id}>{el.content}</div>
      ))}
    </div>
  );
}
