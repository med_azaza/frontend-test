import React, { useState, useEffect } from "react";
import styles from "./Home.module.scss";

export default function Home() {
  const [content, setContent] = useState("0");
  const [history, setHistory] = useState([]);
  useEffect(() => {
    if (window.localStorage.getItem("history") !== null) {
      setHistory(JSON.parse(localStorage.getItem("history")));
    } else {
      localStorage.setItem("history", JSON.stringify([]));
    }
  }, []);
  useEffect(() => {
    localStorage.setItem("history", JSON.stringify(history));
  }, [history]);
  const addValue = (value) => {
    if (
      !(
        ["/", "+", "-", "%", "*"].includes(
          content.charAt(content.length - 1)
        ) && ["/", "+", "-", "%", "*"].includes(value)
      )
    ) {
      if (content === "0") {
        setContent(value);
      } else {
        setContent(content + value);
      }
    }
  };
  const calculate = () => {
    setHistory([
      ...history,
      {
        id: Math.random() * 1000,
        content: content + "=" + eval(content).toString(),
      },
    ]);
    setContent(eval(content).toString());
  };
  const cancel = () => {
    setContent("0");
  };
  return (
    <div className={styles.container}>
      <div className={styles.calcContainer}>
        <div className={styles.header}>
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div className={styles.screen}>{content}</div>
        <div className={styles.btns}>
          <button onClick={() => cancel()} className={styles.dark}>
            {content === "0" ? "AC" : "C"}
          </button>
          <button className={styles.dark}>+/-</button>
          <button onClick={() => addValue("%")} className={styles.dark}>
            %
          </button>
          <button onClick={() => addValue("/")} className={styles.orange}>
            /
          </button>
          <button onClick={() => addValue("7")}>7</button>
          <button onClick={() => addValue("8")}>8</button>
          <button onClick={() => addValue("9")}>9</button>
          <button onClick={() => addValue("*")} className={styles.orange}>
            X
          </button>
          <button onClick={() => addValue("4")}>4</button>
          <button onClick={() => addValue("5")}>5</button>
          <button onClick={() => addValue("6")}>6</button>
          <button onClick={() => addValue("-")} className={styles.orange}>
            -
          </button>
          <button onClick={() => addValue("1")}>1</button>
          <button onClick={() => addValue("2")}>2</button>
          <button onClick={() => addValue("3")}>3</button>
          <button onClick={() => addValue("+")} className={styles.orange}>
            +
          </button>
          <button onClick={() => addValue("0")}>0</button>
          <button onClick={() => addValue(",")}>,</button>
          <button onClick={() => calculate()} className={styles.orange}>
            =
          </button>
        </div>
      </div>
    </div>
  );
}
