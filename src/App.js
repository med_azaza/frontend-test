import React from "react";
import Home from "./pages/homepage";
import History from "./pages/historyPage";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

export default function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/history" component={History} />
        </Switch>
      </Router>
    </>
  );
}
